// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
 
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion, Payload} = require('dialogflow-fulfillment');
 
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  function test(agent) {
    agent.add(new Card({
        title: `Title: this is a card title`,
        imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
        text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
        buttonText: 'This is a button',
        buttonUrl: 'https://assistant.google.com/'
      })  
    );
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  function locationIntent(agent) {
    agent.add(`Are you looking for subsidized day care only?`);
    agent.add(new Card({
        title: `For more info check the subsidized calculator`,
        buttonText: `Link`,
        // buttonUrl: `https://s3.ca-central-1.amazonaws.com/elevatehack/index.html`
        buttonUrl: `https://www.toronto.ca/community-people/employment-social-support/child-family-support/child-care-support/child-care-fee-subsidy-calculator/`
      })
    );
    agent.add(new Suggestion(`subsidized only`));
    agent.add(new Suggestion(`nope, include all`));
  }

  function recommendIntent(agent) {
    agent.add(`Here are the recommended day care according to your preference`);
    agent.add(new Payload(agent.FACEBOOK, {
              "attachment": {
                  "type": "template",
                  "payload": {
                    "template_type":"generic",
                    "elements":[
                       {
                        "title":"Mothercraft Toronto Eaton Centre",
                        "image_url":"https://s3.ca-central-1.amazonaws.com/elevatehack/images/1.jpg",
                        "subtitle":"Mothercraft Toronto Eaton Centre",
                        "default_action": {
                          "type": "web_url",
                          "url": "http://www.mothercraft.ca",
                        },
                        "buttons":[
                          {
                            "type":"web_url",
                            "url":"https://s3.ca-central-1.amazonaws.com/elevatehack/index.html",
                            "title":"View"
                          },{
                            "type":"web_url",
                            "url":"https://www.google.ca/maps/place/Kinder+College+Early+Learning+Centre/@43.6499208,-79.3903266,17z/data=!3m1!4b1!4m10!1m2!2m1!1sdaycare!3m6!1s0x882b34ce18d2e527:0x4bb7c79745678bed!8m2!3d43.6499169!4d-79.3881379!9m1!1b1",
                            "title":"Ratings"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Register"
                          }               
                        ]  
                      },
                      {
                        "title":"YMCA Child Development Centre",
                        "image_url":"https://s3.ca-central-1.amazonaws.com/elevatehack/images/2.jpg",
                        "subtitle":"YMCA Child Development Centre",
                        "default_action": {
                          "type": "web_url",
                          "url": "https://ymcagta.org/child-care",
                        },
                        "buttons":[
                          {
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"View"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Ratings"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Register"
                          }               
                        ]  
                      },
                      {
                        "title":"Queen's Park Child Care Centre",
                        "image_url":"https://s3.ca-central-1.amazonaws.com/elevatehack/images/3.jpg",
                        "subtitle":"Queen's Park Child Care Centre",
                        "default_action": {
                          "type": "web_url",
                          "url": "https://www.qpccc.ca",
                        },
                        "buttons":[
                          {
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"View"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Ratings"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Register"
                          }               
                        ]  
                      },
                      {
                        "title":"Kinder College Early Learning Centre",
                        "image_url":"https://s3.ca-central-1.amazonaws.com/elevatehack/images/4.jpg",
                        "subtitle":"Kinder College Early Learning Centre",
                        "default_action": {
                          "type": "web_url",
                          "url": "http://familycare.utoronto.ca/childcare/child-care-services",
                        },
                        "buttons":[
                          {
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"View"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Ratings"
                          },{
                            "type":"web_url",
                            "url":"http://www.kindercollege.ca/wait-list-application",
                            "title":"Register"
                          }               
                        ]  
                      },
                      {
                        "title":"Orde Day Care Satellite",
                        "image_url":"https://s3.ca-central-1.amazonaws.com/elevatehack/images/5.jpg",
                        "subtitle":"Orde Day Care Satellite",
                        "default_action": {
                          "type": "web_url",
                          "url": "https://www.ordedaycare.org",
                        },
                        "buttons":[
                          {
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"View"
                          },{
                            "type":"web_url",
                            "url":"https://petersfancybrownhats.com",
                            "title":"Ratings"
                          },{
                            "type":"postback",
                            "title":"Call",
                            "payload":"DEVELOPER_DEFINED_PAYLOAD"
                          }               
                        ]  
                      }
                    ]
                  }
              }
          }
      )); 
  }


  // // Uncomment and edit to make your own intent handler
  // // uncomment `intentMap.set('your intent name here', yourFunctionHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function yourFunctionHandler(agent) {
  //   agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
  //   agent.add(new Card({
  //       title: `Title: this is a card title`,
  //       imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
  //       text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
  //       buttonText: 'This is a button',
  //       buttonUrl: 'https://assistant.google.com/'
  //     })
  //   );
  //   agent.add(new Suggestion(`Quick Reply`));
  //   agent.add(new Suggestion(`Suggestion`));
  //   agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
  // }

  // // Uncomment and edit to make your own Google Assistant intent handler
  // // uncomment `intentMap.set('your intent name here', googleAssistantHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function googleAssistantHandler(agent) {
  //   let conv = agent.conv(); // Get Actions on Google library conv instance
  //   conv.ask('Hello from the Actions on Google client library!') // Use Actions on Google library
  //   agent.add(conv); // Add Actions on Google library responses to your agent's response
  // }
  // // See https://github.com/dialogflow/dialogflow-fulfillment-nodejs/tree/master/samples/actions-on-google
  // // for a complete Dialogflow fulfillment library Actions on Google client library v2 integration sample

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('EventFBIntent', test);
  intentMap.set('LocationIntent', locationIntent);
  intentMap.set('RecommendIntent', recommendIntent);

  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);

  agent.handleRequest(intentMap);
});